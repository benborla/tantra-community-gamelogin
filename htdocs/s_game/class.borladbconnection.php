<?php
class BorlaDBConnection {
	private $handler, $connection_status, $query_count,$query_status=false;
	private $host, $db, $username, $password;

	public function __construct($host, $db, $username, $password) {
		$this->host = $host;
		$this->db = $db;
		$this->username = $username;
		$this->password = $password;
		$this->connect();
	}

	public function __destruct() {
		$this->close();
	}

	public function connect() {
		$connectionInfo = array("Database"=>$this->db,"UID"=>$this->username,"PWD"=>$this->password);
		$this->handler = sqlsrv_connect($this->host,$connectionInfo);
		if($this->handler) {
		     $this->connection_status = "Connection established.<br />";
		}else{
		     $this->connection_status =  "Connection could not be established.<br />";
		     #die( print_r( sqlsrv_errors(), true));
		}
	}

	public function close() {
		$this->handler = null;
	}

	public function get_connection_status() {
		return $this->connection_status;
	}

	public function fetch_all($table) { 
		if(!empty($this->handler)) {
			if(!empty($table)) {
				$sql = "Select * from ".$table;
				$stmt = sqlsrv_query($this->handler,$sql);
				$data = array();
				while($row = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
					$data[] = $row;
				}
				return $data;
			}
			else {
				return "Unable to fetch data";
			}

		}
		else {
			echo "Unable to fetch data, database connection is currently closed";
		}
	}
	public function fetch_all_by_query($query) {
		if(!empty($this->handler)) {
			if(!empty($query)) {
				$stmt = sqlsrv_query($this->handler,$query);
				$data = array();
				while($row = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC)){
					$data[] = $row;
				}
				return $data;
			}
			else {
				echo "Query is empty";
			}
		}
		else {
			echo "Unable to fetch data, database connection is currently closed";
		}
	}
	public function get_data($table,$get_value,$reference_name,$reference_value) {
		if(!empty($this->handler)) {
			$sql = "Select * from $table where $reference_name=$reference_value";
			$stmt = sqlsrv_query($this->handler,$sql);
			$row = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
			return $row[$get_value];
		}
		else {
			echo "Unable to fetch data, database connection is currently closed";
		}
	}
	public function get_data_by_query($sql,$get) {
		if(!empty($this->handler)) {
			if(!empty($sql)) {
				$stmt = sqlsrv_query($this->handler,$sql);
				$row = sqlsrv_fetch_array($stmt,SQLSRV_FETCH_ASSOC);
				return $row[$get];
			}
			else {
				echo "Query is empty";
			}
		}
		else {
			echo "Unable to fetch data, database connection is currently closed";
		}
	}
	public function select_db($database) {
		/*
		if(!empty($this->handler)) {
			if(!sqlsrv_query($this->handler,"Use $database")) {
				echo "Unable to change database";
			}
		}
		else {
			echo "Unable to change database, database connection is currently closed";
		}*/
		# reconnect to mssql server again 
		$connectionInfo = array("Database"=>$database,"UID"=>$this->username,"PWD"=>$this->password);
		$this->handler = sqlsrv_connect($this->host,$connectionInfo);
		if($this->handler) {
		     $this->connection_status = "Connection established.<br />";
		}else{
		     $this->connection_status =  "Connection could not be established.<br />";
		     #die( print_r( sqlsrv_errors(), true));
		}
	}
	public function execute_query($query){
		if(!empty($this->handler)) {
			if(!empty($query)) {
				if(sqlsrv_query($this->handler,$query)) {
					$this->query_status = true;
					return true;
				}
				else {
					die( print_r( sqlsrv_errors(), true));
					$this->query_status = false;
					return false;
				}
			}
			else{
				echo "Unable to execute, query is empty";
			}
		}
		else {
			echo "Unable to execute query, connection is currently closed";
		}
	}
	public function get_query_status() { 
		if(!empty($this->handler)) {
			if(!empty($this->query_status)){
				return $this->query_status;
			}
			else {
				return "No query has been executed";
			}
		}
		else {
			return "Database connection is currently closed.";
		}
	}
	public function clean($value) {
		$value = str_replace("'","&#39;",$value);
		$value = str_replace("\"","&quot;",$value);
		$value = str_replace("-", "&ndash;",$value);
		$value = str_replace("<", "&lt;",$value);
		$value = str_replace(">", "&gt;",$value);
		$value = str_replace("/", "&#47;",$value);
		$value = str_replace("!", "&#33;",$value);
		return $value;
	}
}

?>