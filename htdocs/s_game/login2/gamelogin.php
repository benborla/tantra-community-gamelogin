<?php
/*======================================================================*\
|| #################################################################### ||
|| #                        Torzap Web 1.3.0                          # ||
|| # ---------------------------------------------------------------- # ||
|| #              Copyright ©2011-2014 John A. Torres                 # ||
|| #                 Todos los derechos reservados                    # ||
|| # ----------------          Game Login            ---------------- # ||
|| # Torzap1@gmail.com             -           Johnamtorres@gmail.com # ||
|| #################################################################### ||
\*======================================================================*/
require('../config.php');
require('../inc/md5.php');
require('../class.tantradbhelper.php');
function tzguard($var) {
	$words = array ("'", '"', ",", ";", "--", "==", "0x", "</", "@sql", "%s");
	$var = str_ireplace($words, '', $var);
	return $var;
}

$user_id = tzguard($_GET['user_id']);
$user_pass = tzguard($_GET['user_pass']);
$status = '0';


if(SV_MANTTO == 1){
	$status = 4;
} else {
	$tantra = new TantraHelper();
	$R1 = array(
	"Email"=> $tantra->get_login_column($user_id,$user_pass,"Email"),
	"Password"=>$tantra->get_login_column($user_id,$user_pass,"Password"),
	"Activated"=>$tantra->get_login_column($user_id,$user_pass,"Activated"),
	"Blocked"=>$tantra->get_login_column($user_id,$user_pass,"Blocked"),
	"BlockedEndDate"=>$tantra->get_login_column($user_id,$user_pass,"BlockedEndDate"),
	);

	if($R1['Email'] != ''){
		$initial = 'etc';
		$UserID = $R1['Email'];
		$UserPassword = $R1['Password'];
		$Password = strtoupper(trim($UserPassword));
		$initial = substr($UserID,0,1);
		$accountTAD = account_dir()."\\".$initial.'\\'.$UserID.'.tad';
		if($R1['Email'] != '' && $R1['Activated'] == 1){
			if(!file_exists($accountTAD)){
				$userlenght = strlen(trim($UserID));
				$f1 = fopen('../inc/base.tad', 'r');
				$acc = fread($f1,7124);
				$demoid = substr($acc,0,$userlenght);
				$demopass = substr($acc,52,32);
				$acc = str_replace($demoid,$UserID,$acc);
				$acc = str_replace($demopass,$Password,$acc);
				$f2 = fopen($accountTAD, 'a');
				fwrite($f2,$acc);
				fclose($f1);
				fclose($f2);
			}
		}
		if ($R1['Activated'] == 1 && $R1['Blocked'] == 0) {
			$userPass = $R1['Password'];
			$user_pass_ok = strtolower($userPass);
			$user_pass_ok = "@".substr($user_pass_ok,0,1)."^".substr($user_pass_ok,1);
			$user_pass_ok = md5($user_pass_ok);
			if ($user_pass != $user_pass_ok){
				$status = '1';
			}
		} else {
			if($R1['Blocked'] == 1){
				$todayDate = date('d/m/Y H:i:s');
				$BlockedEndDate = date('d/m/Y H:i:s',strtotime($R1['BlockedEndDate']));
				if($todayDate >= $BlockedEndDate){
					$tantra->update_block_date($user_id);
				}
				$status = '3';
			} elseif($R1['Activated'] == 0){
				$status = '6';
			}
		}
	} else {
		$status = '2';
	}

}
	
$url = WEBSITE . "/gamelogin/remote/login/{$user_id}/{$status}";

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL,$url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
curl_exec($ch);

die($status);
?>