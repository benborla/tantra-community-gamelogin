<?php
function add_to_log($msg) {
	$contents = "
$msg";
	file_put_contents("benborla_weblogs/weblogs.borla", $contents, FILE_APPEND);
}
function account_dir() {
	$acct_dir = "C:\\Server\\DBSRV\\account";
	return $acct_dir;
}
function share_dir() {
	$server_share_path = "C:\\Server\\Share\\Serv01\\new\\";
	return $server_share_path;
}
function share_update() {
	$server_share_path_update = "C:\\Server\\Share\\Serv01\\update\\";
	return $server_share_path_update;
}


function ip() {
	return $_SERVER['REMOTE_ADDR'];
}
function base_uri() {
	$base_uri = $_SERVER['SERVER_NAME']."/";
	$protocol = !empty($_SERVER['HTTPS']) ? "https://" : "http://";
	$website = $protocol.$base_uri;
	return $website;
}
function get_404() {
	return base_uri()."404.php";
}
function get_image($filename) {
	return base_uri()."assets/img/".$filename;
}
function __redirect($page) {
	header("location:".base_uri().$page);
}
function server_name() {
	return "Chronicles of Tantra";
}
function server_alias() {
	return "CoT";
}
function get_fb_page() {
	return "https://www.facebook.com/pages/CoT-Chronicles-of-Tantra/688353781292651";
}
##########################################
#				EXTRAS                   #
##########################################
function get_country() {
	$temp = "http://api.hostip.info/country.php";
    $response = file_get_contents($temp);
	return $response;
}
function get_country_flag() {
	return '<IMG SRC="http://api.hostip.info/flag.php" BORDER="0" width="35" height="18" ALT="IP Address Lookup">';
}
function read_file($filename) {
	$f = fopen($filename,"r");
	$value = fread($f, filesize($filename));
	fclose($f);
	return $value;
}
 function format_num($num, $precision = 2) {
	if ($num >= 1000 && $num < 1000000) {
	$n_format = number_format($num/1000,$precision).'K';
	} else if ($num >= 1000000 && $num < 1000000000) {
	$n_format = number_format($num/1000000,$precision).'M';
	} else if ($num >= 1000000000) {
	$n_format=number_format($num/1000000000,$precision).'B';
	} else {
	$n_format = $num;
	}
	return $n_format;
} 
function getRank($num) {
	if ( ! is_numeric($num)) return $num;

	if ($num % 100 >= 11 and $num % 100 <= 13)
	{
		return $num."th";
	}
	elseif ( $num % 10 == 1 )
	{
		return $num."st";
	}
	elseif ( $num % 10 == 2 )
	{
		return $num."nd";
	}
	elseif ( $num % 10 == 3 )
	{
		return $num."rd";
	}
	else
	{
		return $num."th";
	}
}
function generateKey(){
	$key = "borla___".ip2long(ip());
	$ret = txt2hex($key,2);
	return base64_encode($ret);
}
function isKeyValid($id) {
	$value = trim(hex2txt(base64_decode($id),2));
	$exp = explode("___",$value);
	$delim1 = $exp['0'];
	$delim2 = $exp['1'];
	if(strlen($delim1)>0) {
		if($delim1=='borla'){
			$ip = long2ip($delim2);
			if(filter_var($ip,FILTER_VALIDATE_IP)){
				return 0;
			}
			else {
				return 1;
			}
		}
		else {
			return 1;
		}
	}
	else {
		return 1;
	}
}
function getActivationKey() {
	$activecode = sha1(mt_rand().time().mt_rand().ip());
	$activation = substr($activecode,0,16);
	return $activation;
}
function create_captcha() {
	$ccode = sha1(mt_rand().time().mt_rand().ip());
	$captcha_code = substr($ccode,0,6);
	return strtoupper($captcha_code);
}
function getCurrentMonth() {
	$d = Date("Y-m-d");
	return date('F', strtotime($d));
}
function getFDate() {
	$mydate = getdate(date("U"));
	$d = "$mydate[weekday], $mydate[month], $mydate[mday], $mydate[year]";
	return $d;
}
##########################################
#		CRYPTOGRAPHY     		     #
##########################################
function to_hex($txt,$loops) {
	try{
		$value = @bin2hex($txt);
		for($i = 1; $i <= $loops; $i++) {
			$value = @bin2hex($value);
		}
		return strrev($value);
	}catch(Exception $e) { echo ""; }
}
function to_string($hex,$loops) {
	try{
		$value = @pack("H*",strrev($hex));
		for($i = 1; $i <= $loops; $i++) {
			$value = @pack("H*",$value);
		}
		return $value;
	}catch(Exception $e) { echo ""; }
}
function encode($value) {
	return base64_encode($value);
}
function decode($value) {
	return base64_decode($value);
}
function borla_encrypt($value,$loops) {
	$encrypt_this = to_hex($value,$loops);
	$encrypt_this = encode($encrypt_this);
	return $encrypt_this;
}
function borla_decrypt($value,$loops) {
	$decrypt_this = decode($value);
	$decrypt_this = to_string($decrypt_this,$loops);
	return $decrypt_this;
}

##########################################
#			 GAME MASTERS INFO           #
##########################################
$gm_userids = array("USERNAME1", "USERNAME2", "USERNAME3", "USERNAME4", "USERNAME5");

##########################################
#			  DOWNLOAD LINKS             #
##########################################
$mediafire_link = "http://www.mediafire.com/download/l6pftoy69ujdybr/cot-online.exe";
$megaupload_link= "https://mega.co.nz/#!kIBCRCCB!S4X_LQ2isxhxG_Msilr0wAhtuTLoqsv_45XOBLtIy1g";

##########################################
#			   TOP-UP REATES             #
##########################################
$php_price_1 = 100;
$php_price_2 = 300;
$php_price_3 = 500;
$php_price_4 = 1000;

?>