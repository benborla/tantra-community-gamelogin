<?php 
require_once("borla.globals.php");
require_once("class.borladbconnection.php");
require_once("config.php");
class TantraHelper {
	private $db;

	public function __construct() {
	        $this->db = new BorlaDBConnection(DB_HOST,DB_LOGI,DB_USER,DB_PASS);
	}
	public function __destruct() {
		$this->db = null;
	}
	public function get_connection_status() {
		return $this->db->get_connection_status();
	}

	private function clean($value){
		return $this->db->clean($value);
	}

	###################################################
	# 			      USERLOGIN DATABASE              #
	###################################################
	// game login settings
	public function get_login_column($username,$password,$column) {
		$data = array();
		$this->db->select_db("UserLogin");
		$query = "SELECT UserID,Email,Password,Activated,Blocked,BlockedEndDate FROM Account WITH (NOLOCK) WHERE UserID = '".$this->db->clean($username)."'";
		$data = $this->db->get_data_by_query($query,$column);
		return $data;
	}
	public function update_block_date($username) {
		$this->db->execute_query("UPDATE Account SET Blocked = 0, BlockedDate = NULL, BlockedEndDate = NULL WHERE UserID = '".$this->db->clean($username)."' or Email = '" . $this->db->clean($username) . '"');
	}
	public function login_player($username,$password) {
		$this->db->select_db("UserLogin");
		$query = "Select * from Account where UserId='".$this->db->clean($username)."' and Password='".$this->db->clean($password)."'";
		$return_username = $this->db->get_data_by_query($query,"UserID");
		if(!empty($return_username)) {
			return true;
		}
		else {
			return false;
		}
	}
	private function get_by_username($username,$get_column){
		$this->db->select_db("UserLogin");
		return $this->db->get_data("Account",$get_column,"UserID","'".$this->db->clean($username)."'");
	}
	public function get_password($username) {
		$this->db->select_db("UserLogin");
		$password = $this->get_by_username($username,"Password");
		return $password;
	}
	public function get_email($username) {
		$email = $this->get_by_username($username,"Email");
		return $email;
	}
	public function get_first_name($username){
		$fname = $this->get_by_username($username,"Firstname");
		return $fname;
	}
	public function get_last_name($username){
		$lname = $this->get_by_username($username,"Lastname");
		return $lname;
	}
	public function get_ip_address($username) {
		$ip = $this->get_by_username($username,"RegIPAddress");
		return $ip;
	}
	public function username_exists($username) {
		$value = $this->get_by_username($username,"UserID");
		if(!empty($value)) {
			return true;
		}
		else {
			return false;
		}
	}
	public function email_exists($email) {
		$this->db->select_db("UserLogin");
		$value = $this->db->get_data("Account","Email","Email","'".$this->db->clean($email)."'");
		if(!empty($value)) {
			return true;
		}
		else {
			return false;
		}
	}
	/*
	public function get_registration_date($username){
		$dr = $this->get_by_username($username,"DateRegistered");
		return date("Y-m-d",$dr);
	}*/
	public function get_activation_key($username) {
		$activate = $this->get_by_username($username,"ESActivation");
		return $activate;
	}
	public function get_account_status($username){
		$status = $this->get_by_username($username,"Activated");
		$real_status = array("Not Activated","Activated");
		return $real_status[$status];
	}
	# add new player 
	public function register_player($json_account_details) {
		$account = json_decode($json_account_details);
		$email = $this->db->clean($account->email);
		$username = $this->db->clean($account->username);
		$password = $this->db->clean($account->password);
		$firstname = $this->db->clean($account->firstname);
		$lastname = $this->db->clean($account->lastname);
		$address = $this->db->clean($account->address);
		$ip_address = $this->db->clean($account->ip);
		$country = $this->db->clean($account->country);
		$initial = substr($username,0,1);
		if(!file_exists(account_dir()."\\".$initial."\\".$username.".tad")){
			$userkey = "0000000";
			$activation = getActivationKey();
			$password2 = strtoupper(md5($password));
			$userlength = strlen($username);
			$this->db->select_db("UserLogin");
			$sql = "INSERT INTO Account VALUES ('$email', '$username', '$password', '$userkey', '0', NULL, NULL, NULL, '', '', '$firstname', '', '$lastname', '1989-01-01 00:00:00.000', '0', '$address', NULL, NULL, '$country', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '$ip_address', newid(), GETDATE(), 1, NULL, 0, 0, 0, NULL, 0, 1, NULL, 0, 0, NULL, NULL, 0, 0, '$activation')
				";
			if($this->db->execute_query($sql)) {
				$this->db->execute_query("DECLARE	@return_value int,
					@NCashResult int,
					@NCashMsg nvarchar(100)

			EXEC	@return_value = [dbo].[AccountInsertUserInBilling]
					@Email = N'".$email."',
					@NCashResult = @NCashResult OUTPUT,
					@NCashMsg = @NCashMsg OUTPUT");
				# write to server file directory
				$fp = fopen(share_dir()."\\$username.txt","w");
				fwrite($fp,$username."\r\n");
				fwrite($fp,$password."\r\n");
				fwrite($fp,"000000\r\n");
				fwrite($fp,$userkey."\r\n");
				fclose($fp);

				# create account profile
				$f = fopen("../inc/sample.tad","r");
				$acc = fread($f,7124);
				$dUser= substr($acc,0,$userlength);
				$dPass= substr($acc,52,32);
				$acc = str_replace($dUser,$username,$acc);
				$acc = str_replace($dPass,$password2, $acc);
				$f2 = fopen(account_dir()."\\".$initial."\\".strtoupper($username).".TAD", "a");
				fwrite($f2,$acc);
				fclose($f);
				file_put_contents("../benborla_weblogs/reg_logs.txt", $json_account_details." - SUCCESSFUL", FILE_APPEND);
				return true;

			}
			else {
				# if the query fails
				file_put_contents("../benborla_weblogs/reg_logs.txt", $json_account_details." - FAILED", FILE_APPEND);
				return false;
			}
		}
		//return "Email: $email, Username: $username, Password: $password, Name: $firstname $lastname, Country: $country";

	}

	# for updating info
	public function change_password($username,$new_password) {
		$this->db->select_db("UserLogin");
		$password2 = md5(trim($new_password));
		$userlength= strlen(trim($username));
		$initial   = substr($username,0,1);
		
		$f = fopen(account_dir()."\\".$initial."\\".$username.".tad", "r") or die("There was an error upon changing your password, please try again");
		$acc = @fread($f,7124);
		$dPass= substr($acc,52,32);
		$acc = str_replace($dPass,$password2,$acc);
		$f2 = fopen(account_dir()."\\".$initial."\\".$username.".tad", "w");
		if(fwrite($f2,$acc)) {
			@fclose($f);
			$fp = fopen(share_update() ."$username.txt","w");
			fwrite($fp,$username."\r\n");
			fwrite($fp,$new_password."\r\n");
			fwrite($fp,"000000\r\n");
			fwrite($fp,"0000000\r\n");
			fclose($fp);
			$this->db->execute_query("Update Account set Password='".$this->clean($new_password)."' where UserID='".$this->clean($username)."'");
			return true;
		}
		else {
			return false;
		}

	}
	public function change_email($username,$new_email) {
		$this->db->select_db("UserLogin");
		$this->db->execute_query("Update Account set Email='".$this->clean($new_email)."' where UserID='".$this->clean($username)."'");
	}


	###################################################
	# 			      TANTRA DATABASE                 #
	###################################################
	public function get_ranking($limit,$sort) {
		$this->db->select_db("Tantra");
		$sql = "Select Top ".intval($limit)." Cast(UserID as varchar(max)) as UserID,  Cast(CharacterName as varchar(max)) as CharacterName, Cast(BrahmanPoint as bigint) as BrahmanPoint, Cast(CharacterLevel as int) as CharacterLevel, Cast(Trimurity as int) as Trimurity, Cast(Tribe as int) as Tribe  from TantraBackup00 Order by $sort DESC";
		return $this->db->fetch_all_by_query($sql);
	}
	public function get_guilds() {
		$this->db->select_db("Tantra");
		$sql = "Select Distinct GuildName from TantraBackup00 where GuildName<>''";
		return $this->db->fetch_all_by_query($sql);
	}
	public function get_guild_total_mp($guild_name) {
		$this->db->select_db("Tantra");
		$value = $this->db->get_data_by_query("Select SUM(BrahmanPoint) as BrahmanPoint from TantraBackup00 where GuildName= '".$this->db->clean($guild_name)."'","BrahmanPoint");
		return $value;
	}
	public function get_guild_god($guild_name) {
		
	}
	public function get_guild_ranking() {
		$arr_guilds = array();
		$ctr = 0;
		// store guild names in an array
		foreach($this->get_guilds() as $guilds) {
			$arr_guilds[$ctr] = $guilds['GuildName'];
			$ctr++;
		}
		// store guild name and brahman point in a multi-dimensional array
		$arr_guild_mp = array();
		for($x=0;$x<sizeof($arr_guilds);$x++){
			$arr_guild_mp[] = array('GuildName' => $arr_guilds[$x], 'BrahmanPoint' => $this->get_guild_total_mp($arr_guilds[$x]));
		}
		// convert index into variable keys
		foreach ($arr_guild_mp as $key => $row) {
			$guildname[$key]  = $row['GuildName'];
			$brahmanpoint[$key] = $row['BrahmanPoint'];
		}
		// sort brahman points
		array_multisort($brahmanpoint, SORT_DESC,$arr_guild_mp);
		return $arr_guild_mp;
		/*
		foreach($arr_guild_mp as $guilds_mp) {
			echo $guilds_mp["GuildName"].": ".number_format($guilds_mp['BrahmanPoint'])."<br/>";
			$limiter++;
			if($limiter>$limit) {
				break;
			}
			
		}*/
	}
	public function main_character($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(CharacterName as varchar(max)) as CharacterName from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"CharacterName");
		if(empty($value)) {
			// return username 
			return $this->get_first_name($userid);
		}
		else {
			return $value;
		}
		
	}
	public function main_character_level($userid) {
		$this->db->select_db("Tantra");
		$value = $this->db->get_data("TantraBackup00","CharacterLevel","UserID","'".$this->db->clean($userid)."'");
		return $value;
	}
	public function is_db_empty($database, $table) {
		$this->db->select_db($database);
		$value = $this->db->get_data_by_query("Select Count(UserID) as UserID from $table");
		return $value;
	}
	public function main_character_tribe($userid) {
		$this->db->select_db("Tantra");
		if(!empty($this->is_db_empty("Tantra","TantraBackup00"))) {
			$value = $this->db->get_data("TantraBackup00","Tribe","UserID","'".$this->db->clean($userid)."'");
			return $value;
		}
		else {
			return "";
		}

	}	
	public function main_character_trimurity($userid) {
		$this->db->select_db("Tantra");
		$value = $this->db->get_data("TantraBackup00","trimurity","UserID","'".$this->db->clean($userid)."'");
		return $value;
	}
	public function main_character_mp($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(BrahmanPoint as varchar(max)) as BrahmanPoint from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"BrahmanPoint");
		return $value;
	}
	public function first_character($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(Name1 as varchar(max)) as Name1 from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"Name1");
		return $value;
	}
	public function second_character($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(Name2 as varchar(max)) as Name2 from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"Name2");
		return $value;
	}
	public function third_character($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(Name3 as varchar(max)) as Name3 from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"Name3");
		return $value;
	}
	public function first_character_level($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(Level1 as varchar(max)) as Level1 from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"Level1");
		return $value;
	}
	public function second_character_level($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(Level2 as varchar(max)) as Level2 from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"Level2");
		return $value;
	}
	public function third_character_level($userid) {
		$this->db->select_db("Tantra");
		$sql = "Select Cast(Level3 as varchar(max)) as Level3 from TantraBackup00 where UserID='".$this->db->clean($userid)."'";
		$value = $this->db->get_data_by_query($sql,"Level3");
		return $value;
	}
	
	###################################################
	# 			  BILLCRUX_PHIL DATABASE              #
	###################################################
	public function user_current_taney($userid) {
		$this->db->select_db("billcrux_phil");
		$value = $this->db->get_data("tblUserInfo","cashBalance","UserID","'".$this->db->clean($userid)."'");
		return $value;
	}
}
?>